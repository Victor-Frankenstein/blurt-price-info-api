# Blurt Price Info API

BLURT price tracker and API to serve Blurt Wallet.

There is a price tracker written in Python that checks the price from Hive-Engine, Ionomy, and CoinGecko(CoinGecko currently only tracks ProBit) every five minutes and calculates the average and writes it to a JSON file.

There is also an API written in Go that takes requests at the `price_info` endpoint and returns the price info from the JSON file.

Requires Python 3 and Go.

## Building the API

```
go build -o blurt_api .
```

## Running It

To run the tracker:
```
python3 -m price_tracker
```

To run the API once built:
```
GIN_MODE=release ./blurt_api
```