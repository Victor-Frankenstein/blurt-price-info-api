import json
import logging
import requests
from time import sleep


def write_price_json_file(price):
    """Writes the BLURT price to a file named 'blurt_price.json'."""

    with open("blurt_price.json", "w") as out_file:
        json.dump(price, out_file)


def get_hive_price():
    """Returns the price of HIVE from CoinGecko in USD and BTC as a dictionary."""

    url = "https://api.coingecko.com/api/v3/simple/price?ids=HIVE&vs_currencies=USD,BTC"

    try:
        r = requests.get(url)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        logging.info("Failed to fetch HIVE price from CoinGecko API.")
        logging.info(e)
        return None
    else:
        return r.json().get("hive")


def get_btc_price_usd():
    """Returns the price of BTC from CoinGecko in USD"""

    url = "https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd"

    try:
        r = requests.get(url)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        logging.info("Failed to fetch BTC price from CoinGecko API.")
        logging.info(e)
        return None
    else:
        return float(r.json().get("bitcoin").get("usd"))


def get_blurt_price_he():
    """Returns the price of BLURT on Hive-Engine in USD and BTC as a dictionary."""    

    hive_price = get_hive_price()
    url = "https://api.hive-engine.com/rpc/contracts"

    headers = {"content-type": "application/json"}
    payload = json.dumps({
        "jsonrpc": "2.0",
        "id": 0,
        "method": "findOne",
        "params": {
            "contract": "market",
            "table": "metrics",
            "query": {"symbol": "BLURT"},
            "limit": 1000,
            "offset": 0,
        }
    }, ensure_ascii=False).encode("utf-8")

    try:
        r = requests.post(url, data=payload, headers=headers)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        logging.info("Failed to fetch BLURT price from Hive-Engine API.")
        logging.info(e)
        return None
    else:
        blurt_price_hive = float(r.json().get("result").get("lastPrice"))
        return {
            "usd": float(hive_price.get("usd") * blurt_price_hive),
            "btc": float(hive_price.get("btc") * blurt_price_hive)
        }


def get_blurt_price_ionomy():
    """Returns the price of BLURT on Ionomy in USD and BTC as a dictionary."""

    url = "https://ionomy.com/api/v1/public/market-summary?market=btc-blurt"

    try:
        r = requests.get(url)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        logging.info("Failed to fetch HIVE price from CoinGecko API.")
        logging.info(e)
        return None
    else:
        btc_price_usd = get_btc_price_usd()
        blurt_price_btc = float(r.json().get("data").get("price"))
        return {
            "usd": blurt_price_btc * btc_price_usd,
            "btc": blurt_price_btc
        }


def get_blurt_price_coingecko():
    """Returns the price of BLURT from CoinGecko in USD and BTC as a dictionary."""

    url = "https://api.coingecko.com/api/v3/simple/price?ids=BLURT&vs_currencies=USD,BTC"

    try:
        r = requests.get(url)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        logging.info("Failed to fetch BLURT price from CoinGecko API.")
        logging.info(e)
        return None
    else:
        return r.json().get("blurt")

def get_average_blurt_price():
    """Returns the averaged price of BLURT in BTC and USD as a dictionary."""

    prices = []

    he_price = get_blurt_price_he()
    logging.info("HE Price: {}".format(he_price))
    prices.append(he_price)

    ionomy_price = get_blurt_price_ionomy()
    logging.info("Ionomy Price: {}".format(ionomy_price))
    prices.append(ionomy_price)

    cg_price = get_blurt_price_coingecko()
    logging.info("CoinGecko Price: {}".format(cg_price))
    prices.append(cg_price)

    average_price = {"usd": 0, "btc": 0}
    num_of_prices  = 0

    for price in prices:
        if price is not None:
            num_of_prices += 1
            average_price["usd"] += price["usd"]
            average_price["btc"] += price["btc"]

    average_price["usd"] /= num_of_prices
    average_price["btc"] /= num_of_prices

    logging.info("Average Price: {}".format(average_price))

    return average_price

if __name__ == "__main__":

    logging.basicConfig(
        filename="price_tracker.log",
        level=logging.INFO,
        format="%(asctime)s %(levelname)s: %(message)s",
    )
    
    while True:
        logging.info("Fetching prices from APIs.")
        average_blurt_price = get_average_blurt_price()
        price_for_api = {}
        price_for_api["price_usd"] = round(average_blurt_price["usd"], 8)
        price_for_api["price_btc"] = round(average_blurt_price["btc"], 8)
        logging.info("Writing JSON file.")
        write_price_json_file(price_for_api)
        logging.info("Sleeping for five minutes.")
        sleep(5 * 60)